<?php

/**
 * Class Lionwheel_admin_view
 * This is used to display Lionwheel delivery box on admin area
 *
 * @since 1.0.0
 *
 */
class Lionwheel_admin_view
{
	/**
	 * Enqueue Scripts and Localize Script
	 *
	 * @since 1.0.0
	 */
	public function enqueue_scripts($hook)
	{
		global $post_type;
		if (('post.php' == $hook || 'post-new.php' == $hook) && $post_type == 'shop_order') {
			wp_register_script(
				'lionwheel-delivery-admin',
				plugins_url('admin/js/lionwheel-delivery-admin.js', dirname(__FILE__)),
				array('jquery'),
				PLUGIN_VERSION,
				true
			);
			wp_localize_script('lionwheel-delivery-admin', 'lionwheel_delivery', array(
				'ajax_url' => admin_url('admin-ajax.php'),
				'lionwheel_ajax_nonce' => wp_create_nonce("submit_lionwheel_open_ship"),
				'lionwheel_ajax_get_nonce' => wp_create_nonce("submit_lionwheel_get_ship"),
				'lionwheel_ajax_change_nonce' => wp_create_nonce("submit_lionwheel_change_ship"),
				'lionwheel_ajax_reopen_nonce' => wp_create_nonce("lionwheel_reopen_ship"),
				'lionwheel_ajax_loader' => plugins_url('admin/img/reload.gif', dirname(__FILE__)),
				'lionwheel_err_message' => __('We are experiencing a communication error. Please try again later.', 'woo-lionwheel-delivery'),
				'lionwheel_cancel_ship' => __('Cancel shipment', 'woo-lionwheel-delivery'),
				'lionwheel_cancel_ship_ok' => __('Shipment canceled successfully', 'woo-lionwheel-delivery'),
				'lionwheel_reopen_ship' => __('Reopen shipment', 'woo-lionwheel-delivery'),
				'lionwheel_status_1' => __('Open', 'woo-lionwheel-delivery'),
				'lionwheel_status_2' => __('Delivery man on his way', 'woo-lionwheel-delivery'),
				'lionwheel_status_3' => __('Delivered', 'woo-lionwheel-delivery'),
				'lionwheel_status_4' => __('Collected from customer', 'woo-lionwheel-delivery'),
				'lionwheel_status_5' => __('Back from costumer', 'woo-lionwheel-delivery'),
				'lionwheel_status_7' => __('Approved', 'woo-lionwheel-delivery'),
				'lionwheel_status_8' => __('Canceled', 'woo-lionwheel-delivery'),
				'lionwheel_status_9' => __('Second delivery man ', 'woo-lionwheel-delivery'),
				'lionwheel_status_12' => __('On hold', 'woo-lionwheel-delivery'),
				'lionwheel_err_message_code' => __('Error fetching data from Lionwheel. Please check your API Settings on Settings -> Lionwheel Delivery', 'woo-lionwheel-delivery'),
				'lionwheel_err_message_open_code' => __('Error opening shipment in Lionwheel. Please check your API Settings on Settings -> Lionwheel Delivery', 'woo-lionwheel-delivery')
			));
			wp_enqueue_script('lionwheel-delivery-admin');
		}

	}

	/**
	 * Enqueue styles
	 *
	 * @since 1.0.0
	 */
	public function enqueue_styles($hook)
	{
		global $post_type;
		if (('post.php' == $hook || 'post-new.php' == $hook) && $post_type == 'shop_order') {
			wp_register_style(
				'lionwheel-delivery-admin',
				plugins_url('admin/css/lionwheel-delivery-admin.css', dirname(__FILE__)),
				null,
				PLUGIN_VERSION
			);
			wp_enqueue_style('lionwheel-delivery-admin');
		} elseif ('edit.php' == $hook && $post_type == 'shop_order') {
			wp_register_style(
				'lionwheel-delivery-table-admin',
				plugins_url('admin/css/lionwheel-delivery-table-admin.css', dirname(__FILE__)),
				null,
				PLUGIN_VERSION
			);
			wp_enqueue_style('lionwheel-delivery-table-admin');
		}
	}

	/**
	 * Register Meta Boxes
	 *
	 * @since 1.0.0
	 */
	public function meta_boxes()
	{
		add_meta_box(
			'lionwheel-delivery',
			__('Lionwheel Delivery', 'woo-lionwheel-delivery'),
			array($this, 'lionwheel_meta_box_side'),
			'shop_order',
			'side',
			'high'
		);
	}

	/**
	 * Meta box DOM
	 *
	 * @since 1.0.0
	 */
	public function lionwheel_meta_box_side()
	{
		global $woocommerce, $post;
		$order = wc_get_order($post->ID);
		if (function_exists('wc_seq_order_number_pro')) {
			$order_id = $post->ID;
		} else {
			$order_id = $order->get_id();
		}

		$order_date = new DateTime($order->get_date_created());

		$active_date = new DateTime(get_option('lionwheel_install_date'));
		$lionwheel_label_nonce = wp_create_nonce("lionwheel_create_label");
		$lionwheel_order_id_db = get_post_meta( $order_id, '_lionwheel_ship_data');


		if (!empty($lionwheel_order_id_db)) {
			$lionwheel_delivery_number_arry = get_post_meta($order_id, '_lionwheel_ship_data');
			$cancel = get_post_meta($order_id, '_order_canceled');


			$lionwheel_delivery_number = $lionwheel_delivery_number_arry;
			foreach ($lionwheel_delivery_number as $key => $single) {
				$lionwheel_label_query = 'post.php?lionwheel_pdf=create&lionwheel_label_wpnonce=' . $lionwheel_label_nonce . '&order_id=' . $order_id . '&ship_id=' . $key;
				$lionwheel_delivery_time = explode(" ", $single['delivery_time']);

				?>
                <div class="lionwheel-wrapper">
                    <div id="lionwheel_ship_exists" data-order="<?php echo $order_id ?>">
                        <h4><?php _e('Lionwheel shipment details: ', 'woo-lionwheel-delivery'); ?></h4>
                        <p><?php _e('Shipment number:', 'woo-lionwheel-delivery'); ?> <span
                                    class="lionwheel_delivery_id"><?php echo $single['delivery_number']; ?></span>
                        </p>

                    </div>
                    <div class="lionwheel-button-container">
                        <a class="lionwheel-button lionwheel-print-button" target="_blank"
                           data-order="<?php echo $order_id ?>"
                           href="<?php echo $lionwheel_label_query ?>"><?php _e('Print label', 'woo-lionwheel-delivery'); ?></a>
						<?php
						$order_cancel = false;
						if (in_array($single['delivery_number'], $cancel)) {
							$order_cancel = true;
						}
						?>
                        <button <?php if ($order_cancel) {
							echo 'disabled';
						} ?> class="lionwheel-button lionwheel-cancel-ship" data-order-id="<?php the_ID(); ?>"
                             data-shipping-id="<?php echo $single['delivery_number']; ?>">
							<?php
							if ($order_cancel) {
								_e('Shipment canceled', 'woo-lionwheel-delivery');
							} else {
								_e('canceling delivery', 'woo-lionwheel-delivery');
							}
							?>
                        </button>
						<?php add_thickbox(); ?>

                        <a href="#TB_inline?width=200&height=200&inlineId=modal-window-<?php echo $order_id; ?>"
                           class="thickbox ship_status lionwheel-button"
                           data-status="<?php echo $single['delivery_number']; ?>"><?php _e('Delivery status', 'woo-lionwheel-delivery'); ?></a>

                        <div id="modal-window-<?php echo $order_id; ?>" style="display:none;">
                            <p class="lionwheel_ship_open"><?php _e('Receiver name: ', 'woo-lionwheel-delivery'); ?><span
                                        class="lionwheel_receiver_name"></span></p>
                            <p><?php _e('Delivery time:', 'woo-lionwheel-delivery'); ?><span
                                        class="lionwheel_delivery_time"><?php echo $lionwheel_delivery_time[0]; ?></span></p>
                            <p><?php _e('Delivery status:', 'woo-lionwheel-delivery'); ?><span
                                        class="lionwheel_delivery_status"></span></p>
                            <p class="lionwheel_ship_open"><?php _e('Shipped on:', 'woo-lionwheel-delivery'); ?><span
                                        class="lionwheel_shipped_on"></span></p>
                        </div>

                    </div>

                </div>

				<?php
			}
		}
		//else {
		?>
        <div class="lionwheel-wrapper">
            <div id="lionwheel_open_ship">
                <p><?php if ($active_date >= $order_date) {
						_e('Notice: This plugin was installed after this order was made. Please check with Lionwheel if you already opened this delivery. ', 'woo-lionwheel-delivery');
					} ?></p>
                <h4>יצירת משלוח:</h4>
                <div id="lionwheel_checkbox">
                    <input id="lionwheel_return" type="checkbox" name="lionwheel_return"
                           value="2"><?php _e('Double', 'woo-lionwheel-delivery'); ?><br>
                    <div class="collect_wrap hidden" style="margin: 10px 0;">
                        <label for="collect"><?php _e('Amount to collect', 'woo-lionwheel-delivery'); ?></label>
                        <input type="number" name="collect" id="collect">
                    </div>
                    <br>
                    <input type="radio" class="lionwheel_delivey_type" name="lionwheel_delivey_type" value="1"
                           checked><?php _e('Regular delivery', 'woo-lionwheel-delivery'); ?>
                    <br>
                    <input type="radio" class="lionwheel_delivey_type" name="lionwheel_delivey_type"
                           value="2"><?php _e('Collecting', 'woo-lionwheel-delivery'); ?>
                </div>
                <span><?php _e('Packages:', 'woo-lionwheel-delivery'); ?></span><input id="lionwheel_packages" type="number"
                                                                                   name="packages" value="1"><br>
                <span><?php _e('Deliver on:', 'woo-lionwheel-delivery'); ?></span><input id="lionwheel_exaction_date"
                                                                                     type="date" name="date"
                                                                                     value="<?php echo date('Y-m-d') ?>"><br>
                <div class="lionwheel-button-container-open">
                    <button class="lionwheel-button lionwheel-open-button" data-order="<?php echo $order_id ?>">
						<?php _e('Open shipment', 'woo-lionwheel-delivery'); ?></button>
                </div>
            </div>
            <div class="lionwheel-success-ship">
                <p><?php _e('Shipment number:', 'woo-lionwheel-delivery'); ?><span class="lionwheel-success-ship-number"></span>
                </p>
                <div class="lionwheel-button-container">
                    <a class="lionwheel-button lionwheel-print-button" target="_blank" data-order="<?php echo $order_id ?>"
                       href="<?php echo isset($lionwheel_label_query) ? $lionwheel_label_query : ''; ?>"><?php _e('Print label', 'woo-lionwheel-delivery'); ?></a>
                </div>
            </div>
            <div class="lionwheel-powered-by">
                <span>Powered by LionWheel</span><a target="_blank" href="https://lionwheel.co.il"><img
                            src="<?php echo plugins_url('admin/img/logo.png', dirname(__FILE__)) ?>"></a>
            </div>
        </div>
		<?php
		//}
	}

	/**
	 * Shop orders columns head
	 *
	 * @since 1.1
	 */
	public function lionwheel_admin_column_head($columns)
	{
		$columns['lionwheel_delivery_column'] = __('Lionwheel Delivery', 'woo-lionwheel-delivery');
		return $columns;
	}

	/**
	 * Shop orders columns content
	 *
	 * @since 1.1
	 */
	public function lionwheel_admin_column($column, $post_id)
	{
		$order = wc_get_order($post_id);
		if (function_exists('wc_seq_order_number_pro')) {
			$order_id = $post_id;
		} else {
			$order_id = $order->get_id();
		}

		$lionwheel_order_meta = get_post_meta($order_id, '_lionwheel_ship_data');
		$lionwheel_label_nonce = wp_create_nonce("lionwheel_create_label");
		$lionwheel_label_query = 'post.php?lionwheel_pdf=create&lionwheel_label_wpnonce=' . $lionwheel_label_nonce . '&order_id=' . $order_id;
		if ($column == 'lionwheel_delivery_column') {
			if ($lionwheel_order_meta) {
				?>
                <div class="lionwheel-table-deliv-num">
					<?php _e('Delivery number:', 'woo-lionwheel-delivery'); ?>
                    <span><?php echo $lionwheel_order_meta[0]['delivery_number'] ?></span>
                </div>
                <div class="lionwheel-button-container">
                    <a class="lionwheel-button lionwheel-print-button" target="_blank" data-order="<?php echo $order_id ?>"
                       href="<?php echo $lionwheel_label_query ?>"><?php _e('Print label', 'woo-lionwheel-delivery'); ?></a>
                </div>
				<?php
			} else {
				?>
                <div class="lionwheel-table-deliv-not">
					<?php _e('No delivery to display', 'woo-lionwheel-delivery'); ?>
                </div>
				<?php
			}

		}
	}
}

?>