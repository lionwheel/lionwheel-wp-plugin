<?php
    
    /**
     * Class Lionwheel_service
     * This is used to communicate with LionWheel API
     */
    class Lionwheel_service
    {
        
        private $lionwheel_login = array();
        
        public function __construct()
        {
            $this->lionwheel_login['url'] = get_option('lionwheel_service_url', TAPUZ_DEFAULT_URL);
            $this->lionwheel_login['username'] = get_option('lionwheel_username');
            $this->lionwheel_login['password'] = get_option('lionwheel_password');
            $this->lionwheel_login['code'] = get_option('lionwheel_customer_code');
            $this->lionwheel_login['collect_street'] = get_option('lionwheel_collect_street_name');
            $this->lionwheel_login['collect_street_number'] = get_option('lionwheel_collect_street_number');
            $this->lionwheel_login['collect_city'] = get_option('lionwheel_collect_city_name');
            $this->lionwheel_login['collect_company'] = get_option('lionwheel_collect_company_name');
        }
        
        /**
         * Open CURL with Lionwheel API
         * @param $data
         * @param $lionwheel_func
         *
         * @return mixed
         * @throws Exception
         */
        private function lionwheel_connection($data, $lionwheel_func)
        {
            $url = $this->lionwheel_login['url'] . $lionwheel_func;
            
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($curl);
            if ($response === false) {
                throw new Exception('Communication error');
            }
            curl_close($curl);
            return $response;
            
        }
        
        /**
         * Convert response from XML to JSON
         *
         * @param $lionwheel_response
         *
         * @return array
         */
        private function lionwheel_xml_json($lionwheel_response)
        {
            $lionwheel_xml = html_entity_decode($lionwheel_response);
            $xml = new SimpleXMLElement($lionwheel_xml);
            return (array)$xml;
        }
        
        /**
         * Get Lionwheel Ship Status
         * @param $lionwheel_id
         *
         * @return array
         */
        public function get_ship_status($lionwheel_id)
        {
            $data = array();
            $data['customerId'] = $this->lionwheel_login['code'];
            $data['deliveryNumbers'] = $lionwheel_id;
            try {
                $response = $this->lionwheel_connection($data, TAPUZ_GET_BY_ID);
                return $this->lionwheel_xml_json($response);
            } catch (Exception $e) {
                print_r($e->getMessage());
                die();
            }
        }
        
        /**
         * Open Lionwheel ship
         * @param $ship_data
         *
         * @return array
         */
        public function create_ship($ship_data)
        {
            $ship_data['lionwheel_static'] = '0';
            $ship_data['lionwheel_empty'] = '';
            if ($ship_data['type'] == '2') {
                $pParam = $ship_data['type'] . ';'
                . $ship_data['street'] . ';'
                . $ship_data['number'] . ';'
                . $ship_data['city'] . ';'
                . $this->lionwheel_login['collect_street'] . ';'
                . $this->lionwheel_login['collect_street_number'] . ';'
                . $this->lionwheel_login['collect_city'] . ';'
                . $ship_data['contact_name'].' '.$ship_data['contact_phone'].' '.$ship_data['company']  . ';'
                . $this->lionwheel_login['collect_company'] . ';'
                . $ship_data['note'] . ';'
                . $ship_data['urgent'] . ';'
                . $ship_data['lionwheel_static'] . ';'
                . $ship_data['motor'] . ';'
                . $ship_data['packages'] . ';'
                . $ship_data['return'] . ';'
                . $ship_data['lionwheel_static'] . ';'
                . $ship_data['woo_id'] . ';'
                . $this->lionwheel_login['code'] . ';'
                . $ship_data['lionwheel_static'] . ';'
                . $ship_data['extra_note'] . ';'
                . $ship_data['lionwheel_static'] . ';'
                . $ship_data['lionwheel_empty'] . ';'
                . $ship_data['lionwheel_empty'] . ';'
                . $this->lionwheel_login['collect_company']  . ';'
                . $ship_data['contact_phone'] . ';'
                . $ship_data['contact_mail'] . ';'
                . $ship_data['exaction_date'] . ';'
                . $ship_data['collect'];
            } else {
                $pParam = $ship_data['type'] . ';'
                . $this->lionwheel_login['collect_street'] . ';'
                . $this->lionwheel_login['collect_street_number'] . ';'
                . $this->lionwheel_login['collect_city'] . ';'
                . $ship_data['street'] . ';'
                . $ship_data['number'] . ';'
                . $ship_data['city'] . ';'
                . $this->lionwheel_login['collect_company'] . ';'
                . $ship_data['company'] . ';'
                . $ship_data['note'] . ';'
                . $ship_data['urgent'] . ';'
                . $ship_data['lionwheel_static'] . ';'
                . $ship_data['motor'] . ';'
                . $ship_data['packages'] . ';'
                . $ship_data['return'] . ';'
                . $ship_data['lionwheel_static'] . ';'
                . $ship_data['woo_id'] . ';'
                . $this->lionwheel_login['code'] . ';'
                . $ship_data['lionwheel_static'] . ';'
                . $ship_data['extra_note'] . ';'
                . $ship_data['lionwheel_static'] . ';'
                . $ship_data['lionwheel_empty'] . ';'
                . $ship_data['lionwheel_empty'] . ';'
                . $ship_data['contact_name'] . ';'
                . $ship_data['contact_phone'] . ';'
                . $ship_data['contact_mail'] . ';'
                . $ship_data['exaction_date'] . ';'
                . $ship_data['collect'];
            }
            $data_send = array();
            $data_send['pParam'] = $pParam;
            try {
	            error_log( sprintf( 'Lionwheel shipping data: %s', print_r( $data_send, true ) ) );

                $lionwheel_response = $this->lionwheel_connection($data_send, TAPUZ_SAVE_NEW);
                return $this->lionwheel_xml_json($lionwheel_response);
            } catch (Exception $e) {
                print_r($e->getMessage());
                die();
            }
        }
        
        public function change_ship_status($lionwheel_id)
        {
            $data = array();
            $data['p1'] = $this->lionwheel_login['code'];
            $data['p2'] = $lionwheel_id;
            $data['p3'] = '8';
            try {
                $response = $this->lionwheel_connection($data, TAPUZ_CHANGE_STATUS);
                return $this->lionwheel_xml_json($response);
            } catch (Exception $e) {
                print_r($e->getMessage());
                die();
            }
        }
    }
    
    
