<?php
/**
 * Create a sub menu in WordPress settings menu
 */
// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

function register_lionwheel_menu() {

	add_options_page(
		__( 'Lionwheel Delivery', 'woo-lionwheel-delivery' ),
		__( 'Lionwheel Delivery', 'woo-lionwheel-delivery' ),
		'manage_options',
		'lionwheel-delivery-options',
		'lionwheel_plugin_options'
	);
}

function lionwheel_plugin_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.', 'woo-lionwheel-delivery' ) );
	}
	if ( isset( $_POST['lionwheel_fields_submitted'] ) && $_POST['lionwheel_fields_submitted'] == 'submitted' ) {
		$retrieved_nonce = $_REQUEST['_wpnonce'];
		if (!wp_verify_nonce($retrieved_nonce, 'submit_lionwheel_settings' ) ) die( 'Failed security check' );
		foreach ( $_POST as $key => $value ) {
			if ($key == 'lionwheel_service_url') {
				if (empty($value)) {
					update_option( $key, TAPUZ_DEFAULT_URL);
				} elseif (substr($value, -1) != '/'){
					$with_slash = $value .'/';
					update_option( $key, $with_slash );
				} else {
					update_option( $key, $value );
				}
			}elseif ( get_option( $key ) != $value ) {
				update_option( $key, $value );

			} else {
				add_option( $key, $value, '', 'no' );
			}
		}
		?>
		<div class="notice notice-success is-dismissible">
			<p><?php _e( 'Your settings have been saved.', 'woo-lionwheel-delivery' ); ?></p>
		</div>
	<?php } ?>
	<div class="wrap">
	<h1><?php _e( 'Lionwheel Delivery Integration Settings', 'woo-lionwheel-delivery' ) ?></h1>
	<form class="lionwheel-setting-form" method="POST">
		<h2><?php _e( 'API Settings:', 'woo-lionwheel-delivery' ) ?></h2>
		<?php wp_nonce_field('submit_lionwheel_settings'); ?>
		<span><?php _e( 'Service URL: ', 'woo-lionwheel-delivery' ) ?></span>
			<input type="url" name="lionwheel_service_url" style="width: 40%; max-width: 470px;" value="<?php if (!get_option( 'lionwheel_service_url' )){echo TAPUZ_DEFAULT_URL; } else { echo get_option( 'lionwheel_service_url' ); } ?>"><br><br>
		<span><?php _e( 'Customer code: ', 'woo-lionwheel-delivery' ) ?></span>
			<input type="text" name="lionwheel_customer_code" value="<?php echo get_option('lionwheel_customer_code')?>"><br><br>
		<span><?php _e( 'Username: ', 'woo-lionwheel-delivery' ) ?></span>
			<input type="text" name="lionwheel_username" value="<?php echo get_option('lionwheel_username')?>"><br><br>
		<span><?php _e( 'Password: ', 'woo-lionwheel-delivery' ) ?></span>
			<input type="password" name="lionwheel_password" value="<?php echo get_option('lionwheel_password')?>"><br><br>
		<input type="hidden" name="lionwheel_fields_submitted" value="submitted">
		<h2><?php _e( 'Shipping Label Settings:', 'woo-lionwheel-delivery' ) ?></h2>
		<span><?php _e( 'Label size: ', 'woo-lionwheel-delivery' ) ?></span>
		<select name="lionwheel_paper_size" id="lionwheel_paper_size">
			<option value="A4" <?php if (get_option('lionwheel_paper_size') == 'A4') echo 'selected';?> >A4</option>
			<option value="1904" <?php if (get_option('lionwheel_paper_size') == '1904') echo 'selected';?>>DYMO 99014</option>
			<option value="A4-logo" <?php if (get_option('lionwheel_paper_size') == 'A4-logo') echo 'selected';?>>A4 with logo</option>
		</select><br><br>
		<div id="lionwheel_logo_url" style="display: none">
		<span><?php _e( 'Logo URL: ', 'woo-lionwheel-delivery' ) ?></span>
			<input id="lionwheel_logo_input" type="url" name="lionwheel_logo_url" style="width: 40%; max-width: 470px;" value="<?php  echo get_option( 'lionwheel_logo_url' ) ?>"><br><br>
		</div>
		<h2><?php _e( 'Delivery Settings - collect from address:', 'woo-lionwheel-delivery' ) ?></h2>
		<span><?php _e( 'Street name: ', 'woo-lionwheel-delivery' ) ?></span>
			<input type="text" name="lionwheel_collect_street_name" value="<?php echo get_option('lionwheel_collect_street_name')?>">
		<span><?php _e( 'House number: ', 'woo-lionwheel-delivery' ) ?></span>
			<input type="text" name="lionwheel_collect_street_number" value="<?php echo get_option('lionwheel_collect_street_number')?>"><br><br>
		<span><?php _e( 'City name: ', 'woo-lionwheel-delivery' ) ?></span>
			<input type="text" name="lionwheel_collect_city_name" value="<?php echo get_option('lionwheel_collect_city_name')?>"><br><br>
		<span><?php _e( 'Company name: ', 'woo-lionwheel-delivery' ) ?></span>
			<input type="text" name="lionwheel_collect_company_name" style="width: 30%; max-width: 370px;" value="<?php echo get_option('lionwheel_collect_company_name')?>"><br><br>
		<?php submit_button(__( 'submit', 'woo-lionwheel-delivery'  ), 'primary');?>

		</div>
		<script type="text/javascript">
			window.onload = function() {
				var eSelect = document.getElementById('lionwheel_paper_size');
				var optOtherReason = document.getElementById('lionwheel_logo_url');
				if(eSelect.selectedIndex === 2) {
					optOtherReason.style.display = 'block';
				} else {
					optOtherReason.style.display = 'none';
				}
				eSelect.onchange = function() {
					if(eSelect.selectedIndex === 2) {
						optOtherReason.style.display = 'block';
					} else {
						document.getElementById('lionwheel_logo_input').value = "";
						optOtherReason.style.display = 'none';
					}
				}
			}
		</script>
		<?php

}
