<?php

/**
 * Fired when the plugin is uninstalled.

 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 */

// If uninstall not called from WordPress, then exit.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

delete_option('lionwheel_service_url');
delete_option('lionwheel_customer_code');
delete_option('lionwheel_username');
delete_option('lionwheel_password');
delete_option('lionwheel_paper_size');
delete_option('lionwheel_collect_street_name');
delete_option('lionwheel_collect_street_number');
delete_option('lionwheel_collect_city_name');
delete_option('lionwheel_collect_company_name');
delete_option( 'lionwheel_logo_url' );
